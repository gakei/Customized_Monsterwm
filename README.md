Customized Monsterwm
====================

Fork of original c00kiemo5ter monsterwm (https://github.com/c00kiemon5ter/monsterwm).

Additions
---------

	* Sticky mode;
	* User toggable fullscreen mode;
	* Centre window in the middle and in the middle of the left/right part of the screen;
	* vertgrid layout (2 vertical columns);
	* Fixed wine delay on window close;
	* Something else i don't remember right now.

Warning! The changes were made for personal use, you might need to change some code.

In particular when spawning a new window I had to add some rules for specific applications
which didn't use WM\_CLASS. I also added a check for WM\_NAME for the same reason.

