/* see LICENSE for copyright and license */

#ifndef CONFIG_H
#define CONFIG_H

/** modifiers **/
#define MOD1            Mod1Mask    /* ALT key */
#define MOD4            Mod4Mask    /* Super/Windows key */
#define CONTROL         ControlMask /* Control key */
#define SHIFT           ShiftMask   /* Shift key */

/** generic settings **/
#define MASTER_SIZE     0.50
#define SHOW_PANEL      False     /* show panel by default on exec */
#define TOP_PANEL       True      /* False means panel is on bottom */
#define PANEL_HEIGHT    0         /* 0 for no space for panel, thus no panel */
#define DEFAULT_MODE    TILE      /* initial layout/mode: TILE MONOCLE BSTACK GRID VERTGRID FLOAT */
#define ATTACH_ASIDE    True      /* False means new window is master */
#define FOLLOW_WINDOW   False     /* follow the window when moved to a different desktop */
#define FOLLOW_MOUSE    False     /* focus the window the mouse just entered */
#define CLICK_TO_FOCUS  True      /* focus an unfocused window when clicked  */
#define FOCUS_BUTTON    Button1   /* mouse button to be used along with CLICK_TO_FOCUS */
#define BORDER_WIDTH    1         /* window border width */
#define FOCUS           "#6A8969" /* focused window border color    */
#define UNFOCUS         "#1A221F" /* unfocused window border color  */
#define STICKY          "#8ea8a2" /* sticky window border color  */
#define MINWSZ          50        /* minimum window size in pixels  */
#define DEFAULT_DESKTOP 2         /* the desktop to focus initially */
#define DESKTOPS        10        /* number of desktops - edit DESKTOPCHANGE keys to suit */
#define USELESSGAP      0         /* the size of the useless gap in pixels */
#define MONOCLEBORDER   False     /* show or hide the border in monocle windows */


/**
 * open applications to specified desktop with specified mode.
 * if desktop is negative, then current is assumed
 */
static const AppRule rules[] = { \
    /* Olliolli, Rockets and Whatsapp have exceptions in 
    monsterwm.c */
    /*  class     desktop  follow  float */
    { "Gimp",       -1,    False,  True  },
    { "Wine",       -1,    False,  True  },
    { "Steam",      -1,    False,  True  },
    { "dota2",      -1,    False,  True  },
    { "limbo",      -1,    False,  True  },
    { "imfloat",    -1,    False,  True  },
    { "whatsie",     1,    False,  False },
    { "spotify",    -1,    False,  True},
    { "witcher2",   -1,    False,  True  },
    { "Telegram",    1,    False,  False },
    { "fontforge",  -1,    False,  True  },
    { "TombRaider",           -1, False,  True  },
    { "launchgames",          -1, False,  True  },
    { "explorer.exe",         -1, False,  True  },
    { "lxappearance",         -1, False,  True  },
    { "mainwindow.py",        -1, False,  True  },
    { "LifeIsStrange",        -1, False,  True  },
    { "LolClient.exe",        -1, False,  True  },
	{ "Battle.net.exe",		  -1, False,  True  },
    { "MercenaryKings",       -1, False,  True  },
    { "desktop-launcher",     -1, False,  True  },
    { "telegram-desktop",    1,    False,  False },
    { "retrocityrampage",     -1, False,  True  },
    { "LoLPatcherUx.exe",     -1, False,  True  },
    { "insurgency_linux",     -1, False,  True  },
    { "FeralLinuxMessage",    -1, False,  True  },
    { "qemu-system-x86_64",   -1, False,  True  },
    { "XCOM: Enemy Unknown",  -1, False,  True  },
    { "rads_user_kernel.exe", -1, False,  True  },
    { "League of Legends.exe",-1, False,  True  }

};

/* helper for spawning shell commands */
#define SHCMD(cmd) {.com = (const char*[]){"/bin/sh", "-c", cmd, NULL}}

/**
 * custom commands
 * must always end with ', NULL };'
 */
static const char *termcmd[] = { "urxvtc",     NULL };
static const char *floaterm[]= { "urxvtc", "-name", "imfloat", NULL };
static const char *browser[] = { "/home/gakei/scripts/bin/qutebrowser", NULL };
static const char *volup[]   = { "/home/gakei/scripts/buttons/volume", "up", NULL };
static const char *voldown[] = { "/home/gakei/scripts/buttons/volume", "down", NULL };
static const char *voltoggle[] = { "/home/gakei/scripts/buttons/volume", "toggle", NULL };
static const char *scrot[]    = { "/home/gakei/scripts/utility/scrot", NULL };
static const char *next[]    = { "/home/gakei/scripts/buttons/music", "next", NULL };
static const char *prev[]    = { "/home/gakei/scripts/buttons/music", "prev", NULL };
static const char *stop[]    = { "/home/gakei/scripts/buttons/music", "stop", NULL };
static const char *toggle[]  = { "/home/gakei/scripts/buttons/music", "toggle", NULL };
static const char *games[]   = { "urxvtc", "-name", "launchgames", "-e", "/home/gakei/scripts/utility/gamelaunch/main", NULL };
static const char *touch[]   = { "/home/gakei/scripts/buttons/touch", NULL };
static const char *datenotif[] = { "/home/gakei/scripts/utility/notifydaemon/signal", NULL };
static const char *brightup[] = { "/home/gakei/scripts/utility/backlight/backlight", "+100",  NULL };
static const char *brightdown[] = { "/home/gakei/scripts/utility/backlight/backlight", "-100",  NULL };

#define DESKTOPCHANGE(K,N) \
    {  MOD1,             K,              change_desktop, {.i = N}}, \
    {  MOD1|ShiftMask,   K,              client_to_desktop, {.i = N}},

/**
 * keyboard shortcuts
 */
static Key keys[] = {
    /* modifier          key            function           argument */
    {  MOD1,             XK_b,          togglepanel,       {NULL}},
    {  MOD1,             XK_BackSpace,  focusurgent,       {NULL}},
	{  MOD1,             XK_q, 			killclient,        {NULL}},
    {  MOD1,             XK_j,          next_win,          {NULL}},
    {  MOD1,             XK_k,          prev_win,          {NULL}},
    {  MOD1|SHIFT,       XK_Return,     swap_master,       {NULL}},
    {  MOD1|SHIFT,       XK_j,          move_down,         {NULL}},
    {  MOD1|SHIFT,       XK_k,          move_up,           {NULL}},
    {  MOD1|SHIFT,       XK_s,          switch_mode,       {.i = TILE}},
    {  MOD1|SHIFT,       XK_m,          switch_mode,       {.i = MONOCLE}},
    {  MOD1|SHIFT,       XK_r,          switch_mode,       {.i = BSTACK}},
    {  MOD1|SHIFT,       XK_u,          switch_mode,       {.i = GRID}},
    {  MOD1|SHIFT,       XK_v,          switch_mode,       {.i = VERTGRID}},
    {  MOD1|SHIFT,       XK_f,          switch_mode,       {.i = FLOAT}},
    {  MOD1,             XK_s,          togglefloat,       {.i=1}},
    {  MOD1,             XK_d,          togglefloat,       {.i=0}},
    {  MOD1,             XK_a,          togglesticky,      {NULL}},
    {  MOD1,             XK_f,          fullscreen,        {NULL}},
    {  MOD1,             XK_x,          realfullscreen,    {NULL}},
    {  MOD1,             XK_c,          center,            {.i = 0}},
    {  MOD1|SHIFT,       XK_c,          center,            {.i = 2}},
    {  MOD4|SHIFT,       XK_c,          center,            {.i = 1}},
    {  MOD4|MOD1|SHIFT,  XK_e,     		quit,              {.i = 0}}, /* quit with exit value 0 */
    {  MOD1,             XK_Return,     spawn,             {.com = termcmd}},
    {  MOD4,             XK_Return,     spawn,             {.com = floaterm}},
    {  MOD1|SHIFT,       XK_b,          spawn,             {.com = browser}},
    {  MOD1,             XK_n,          spawn,             {.com = datenotif}},
    {  MOD1|SHIFT,       XK_p,          spawn,             {.com = games}},
    {  MOD1,             XK_F12,        spawn,             {.com = scrot}},
    {  MOD4,             XK_F12,        spawn,             {.com = volup}},
    {  MOD4,             XK_F11,        spawn,             {.com = voldown}},
    {  MOD4,             XK_F10,        spawn,             {.com = voltoggle}},
    {  MOD4,             XK_F5,         spawn,             {.com = brightdown}},
    {  MOD4,             XK_F6,         spawn,             {.com = brightup}},
    {  MOD4,             XK_Right,      spawn,             {.com = next}},
    {  MOD4,             XK_Control_R,  spawn,             {.com = next}},
    {  MOD4,             XK_Left,       spawn,             {.com = prev}},
    {  MOD4,             XK_Alt_R,      spawn,             {.com = prev}},
    {  MOD4,             XK_Down,       spawn,             {.com = toggle}},
    {  MOD4,             XK_Menu,       spawn,             {.com = toggle}},
    {  MOD4,             XK_Up,         spawn,             {.com = stop}},
    {  MOD4,             XK_Shift_R,    spawn,             {.com = stop}},
    {  MOD4,             XK_F9,         spawn,             {.com = touch}},
    {  MOD4,             XK_j,          moveresize,        {.v = (int []){   0,  25,   0,   0 }}}, /* move down  */
    {  MOD4,             XK_k,          moveresize,        {.v = (int []){   0, -25,   0,   0 }}}, /* move up    */
    {  MOD4,             XK_l,          moveresize,        {.v = (int []){  25,   0,   0,   0 }}}, /* move right */
    {  MOD4,             XK_h,          moveresize,        {.v = (int []){ -25,   0,   0,   0 }}}, /* move left  */
    {  MOD4|SHIFT,       XK_j,          moveresize,        {.v = (int []){   0,   0,   0,  25 }}}, /* height grow   */
    {  MOD4|SHIFT,       XK_k,          moveresize,        {.v = (int []){   0,   0,   0, -25 }}}, /* height shrink */
    {  MOD4|SHIFT,       XK_l,          moveresize,        {.v = (int []){   0,   0,  25,   0 }}}, /* width grow    */
    {  MOD4|SHIFT,       XK_h,          moveresize,        {.v = (int []){   0,   0, -25,   0 }}}, /* width shrink  */
       DESKTOPCHANGE(    XK_1,                             0)
       DESKTOPCHANGE(    XK_2,                             1)
       DESKTOPCHANGE(    XK_3,                             2)
       DESKTOPCHANGE(    XK_4,                             3)
       DESKTOPCHANGE(    XK_5,                             4)
       DESKTOPCHANGE(    XK_6,                             5)
       DESKTOPCHANGE(    XK_7,                             6)
       DESKTOPCHANGE(    XK_8,                             7)
       DESKTOPCHANGE(    XK_9,                             8)
       DESKTOPCHANGE(    XK_0,                             9)
};

/**
 * mouse shortcuts
 */
static Button buttons[] = {
    {  MOD1,    Button1,     mousemotion,   {.i = MOVE}},
    {  MOD1,    Button3,     mousemotion,   {.i = RESIZE}},
};
#endif

/* vim: set expandtab ts=4 sts=4 sw=4 : */
